/*

Tuba.hx

Game manager / app launcher

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.nme.tuba;

import nme.Lib;
import nme.events.Event;
import nme.display.Sprite;
import nme.display.Graphics;
import nme.display.StageAlign;
import nme.display.StageScaleMode;
import nme.display.StageDisplayState;
import nme.ui.Mouse;

import nme.display.StageQuality;

import fi.zuigeproductions.nme.tuba.Game;

class Tuba extends Sprite {

	public static var TITLE:String = "Project Tuba";
	public static var VERSION:String = "0.0.1 Pre-Alpha";
	
	public static var FULLSCREEN:Bool = true;
	public static var HIDE_MASK:Bool = false;
	
	public static var GAME_WIDTH:Int = 480;
	public static var GAME_HEIGHT:Int = 270;
	public static var GAME_FPS:Int = 60;
	
	private var game:Game;
	private var frame:Sprite;
	
	public function new() {
	
		super();
		initalize();
	}
	
	private function initalize():Void {
		
		//TODO: Main menu class
		
		//creates the game instance
		game = new Game(GAME_WIDTH, GAME_HEIGHT, GAME_FPS);
		this.addChild(game);
		
		frame = new Sprite();
		this.addChild(frame);
		
		//makes flash perform a little better for some reason
		Lib.current.stage.quality = StageQuality.LOW;
		
		#if !html5
		
		//sets up fullscreen
		if(FULLSCREEN) {
			Mouse.hide();
			Lib.current.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
		}
		
		#end
		
		resize();
		Lib.current.stage.addEventListener (Event.RESIZE, onResize);
		
	}
	
	private function onResize(e:Event):Void {
		
		resize();
		
	}
	
	private function resize():Void {
		
		if( Lib.current.stage.stageWidth / Lib.current.stage.stageHeight < GAME_WIDTH / GAME_HEIGHT) {
			game.scaleX = game.scaleY = Math.floor(Lib.current.stage.stageWidth/GAME_WIDTH);
		} 
		
		else {
			game.scaleX = game.scaleY = Math.floor(Lib.current.stage.stageHeight/GAME_HEIGHT);
		}
		
		//if(game.scaleX < 2) 
			//game.scaleX = game.scaleY = 2;

		//render black bars
		
		game.x = Lib.current.stage.stageWidth/2 - GAME_WIDTH*game.scaleX / 2;
		game.y = Lib.current.stage.stageHeight/2 - GAME_HEIGHT*game.scaleX / 2;
		
	  	if(!HIDE_MASK) {
		
			frame.graphics.clear();
			frame.graphics.beginFill(0x000000);
			frame.graphics.drawRect(0, 0, Lib.current.stage.stageWidth, game.y); 
			frame.graphics.drawRect(0, game.y + GAME_HEIGHT * game.scaleX,  Lib.current.stage.stageWidth, Lib.current.stage.stageHeight);
			frame.graphics.drawRect(0, game.y, game.x, GAME_HEIGHT * game.scaleX);
			frame.graphics.drawRect(game.x + GAME_WIDTH * game.scaleX, game.y,  game.x, GAME_HEIGHT * game.scaleX);
			frame.graphics.endFill();

		}
		
	}
	
	//start of program
	public function main():Void {
		stage.addChild(new Tuba());
	}

}
