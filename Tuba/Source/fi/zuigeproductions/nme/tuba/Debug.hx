/*

Debug.hx

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013


*/

package fi.zuigeproductions.nme.tuba;

import nme.Lib;
import nme.display.Sprite; 
import nme.display.Bitmap;
import nme.display.BitmapData;

import nme.geom.Point;

import nme.text.TextField;
import nme.text.TextFormat;
import nme.text.Font;
import nme.text.AntiAliasType;

import fi.zuigeproductions.nme.tuba.Tuba;

class DebugPanel extends Sprite {
		
	public var title:String  = "<no title>";
	public var version:String = "0.0";
	public var fps:Int = 0;
	public var chunks:Int = 0;
	public var grounded:Bool = false;
	public var hugging:Bool = false;
	
	public var playerPosition:Point;
	
	private var gameInfo:TextField;
	private var dropShadow:TextField;

	public function new () {
	
		super();
		
		gameInfo = new TextField();
		dropShadow = new TextField();
		
		gameInfo.width = dropShadow.width = 200;
		
		dropShadow.defaultTextFormat = new TextFormat("Assets/lowres", 16, 0);
		gameInfo.defaultTextFormat = new TextFormat("Assets/lowres", 16, 0xffffff);
		
		#if flash
		
		gameInfo.embedFonts = dropShadow.embedFonts = true;
		gameInfo.antiAliasType = dropShadow.antiAliasType = AntiAliasType.NORMAL;
		
		#elseif mac
		
		dropShadow.defaultTextFormat = new TextFormat("Assets/lowres", 8, 0);
		gameInfo.defaultTextFormat = new TextFormat("Assets/lowres", 8, 0xffffff);
		
		#end
		
		addChild(dropShadow);
		addChild(gameInfo);
		
		dropShadow.x = gameInfo.x + 1;
		dropShadow.y = gameInfo.y + 1;
			
	}
	
	public function render ():Void {
		
		gameInfo.text = "";
		gameInfo.text += title + " " + version + " \n";
		gameInfo.text += fps + " fps"  + " \n";
		gameInfo.text += "x: " + playerPosition.x + " \n";
		gameInfo.text += "y: " + playerPosition.y + " \n";
		gameInfo.text += "c: " + chunks  + " \n";
		gameInfo.text += "g: " + grounded  + ", h: " + hugging;

		dropShadow.text = gameInfo.text;
		
	}

}
 
class Console extends Sprite {
	
	private var bg:Bitmap;
	public var consolefield:ConsoleField;
	
	private static var bgColour:Int = 0x99000000;
	
	public function new () {
	
		super();
		
		bg = new Bitmap(new BitmapData(200, 252, true, bgColour));
		
		addChild(bg);
		
		consolefield = new ConsoleField();
		consolefield.width = bg.width;
		consolefield.height = bg.height;
		addChild(consolefield);
	
	}

}

class ConsoleField extends TextField {

	public var fontsize:Int = 8;
	private var lines:Array<Dynamic>;
	
	public function new () {

		super();
		
		setRedirection();
		
		lines = new Array();
		
		defaultTextFormat = new TextFormat("Assets/lowres", fontsize*2, 0xffffff);
		
		#if flash
		
		embedFonts = true;
		antiAliasType = AntiAliasType.NORMAL;
		
		#elseif mac
		
		defaultTextFormat = new TextFormat("Assets/lowres", fontsize, 0xffffff);
		
		#end	
		
		wordWrap = true;
	
	}
	
	public function setRedirection():Void {
        haxe.Log.trace = addLine;
    }
	
	public function addLine(line:Dynamic, ?infos:haxe.PosInfos):Void {
	
		lines.push(line);
		render();
	
	}
	
	private function render():Void {
	
		text = "";
		for(i in 0...Math.floor(height/fontsize)) {
			if (lines[lines.length - i - 1] == null) break;
			
			text += lines[lines.length - i - 1] + "\n";
		
		}
	
	}

}