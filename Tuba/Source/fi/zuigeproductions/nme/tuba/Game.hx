/*

Game.hx

Main game class

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013


*/

package fi.zuigeproductions.nme.tuba; 

import nme.Lib;
import nme.Assets;

import fi.zuigeproductions.nme.tuba.SpriteSheet;
import fi.zuigeproductions.nme.tuba.Map;
import fi.zuigeproductions.nme.tuba.Player;
import fi.zuigeproductions.nme.tuba.Controls;
import fi.zuigeproductions.nme.tuba.Debug;

import nme.display.Sprite;
import nme.display.Graphics;
import nme.display.GraphicsGradientFill;
import nme.display.GradientType;
import nme.display.SpreadMethod;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.ui.Keyboard;
import nme.geom.Point;
import nme.geom.Rectangle;
import nme.geom.Matrix;

class Game extends Sprite {
	
	public static var MAP_PATH:String = "assets/map.png"; 
	public static var SPRITES_PATH:String = "assets/spritesheet.png";
	public static var TILE_SIZE:Int = 16; 
	public static var RENDER_SCALE:Float = 1;
	
	public static var GRAVITY:Float = .16;

	public var running:Bool = false;
	public var paused:Bool = false;
	public var console:Bool = false;
	public var debug:Bool = true;
	
	public var gameWidth:Int;
	public var gameHeight:Int;
	public var gameFps:Int;
	
	public var frames:Int;
	
	public var traceConsole:Console;
	public var debugPanel:DebugPanel;
	
	private var fps:Int;
	private var fpsframes:Int;
	private var fpstime:Int;
	
	private static var sprites:SpriteSheet;
	
	private var bg:Sprite;
	private var cam:Sprite;
	private var hud:Sprite;
	
	private var world:Sprite;
	private var map:Map;
	
	private var controls:Controls;
	
	private var camPos:Point;
	private var camVel:Point;
	private var camRect:Rectangle;
	private var renderRect:Rectangle;

	private var player:Player;
	
	public function new (width:Int, height:Int, fps:Int) {
	
		super();
		
		gameWidth = width;
		gameHeight = height;
		gameFps = fps;
		
		addEventListener (Event.ADDED_TO_STAGE, initalize);
		
	}	
	
	private function initalize(e:Event):Void {
	
		addEventListener (Event.ENTER_FRAME, onEnterFrame);
		
		sprites = new SpriteSheet(SPRITES_PATH, TILE_SIZE);
	
		frames = 0;
		fpsframes = 0;
		fpstime = Lib.getTimer();
		
		map = new Map(MAP_PATH, sprites);
		
		bg = new Sprite();
		cam = new Sprite();
		world = new Sprite();
		hud = new Sprite();
		traceConsole = new Console();
		debugPanel = new DebugPanel();
		
		controls = new Controls();
		
		this.addChild(controls);
		this.addChild(bg);	
		this.addChild(cam);
		this.addChild(hud);
		
		cam.addChild(world);
		
		world.addChild(map);
		
		hud.addChild(debugPanel);
		hud.addChild(traceConsole);
		
		debugPanel.visible = debug;
		traceConsole.visible = console;
		traceConsole.x = traceConsole.y = 10;
		
		debugPanel.title = Tuba.TITLE;
		debugPanel.version = Tuba.VERSION;
		
		trace(debugPanel.title + " " + debugPanel.version + " \n");

	}
	
	private function onEnterFrame(e:Event):Void {
	
		frames++;
		
		//trace(Lib.getTimer());
		
		if((Lib.getTimer() - fpstime) >= 1000 ) {
			fpstime += 1000;
			fps = frames - fpsframes;
			fpsframes = frames;
			displayFrames();
		}
		
		if(!running && map.loaded) {
			running = true;
			buildGame();
		}
		
		if(running) {
			
			if(!paused) {
			
				player.controlPlayer(controls);
				player.move();
				player.render();
				
				//make camera follow player
				
				camVel.x = .04 * (player.x + player.look.x * 60 + player.vel.x * 24 - camPos.x);
				camVel.y = .07 * (player.y + player.look.y * 40 + player.vel.y * 6 - camPos.y);
				
				camPos = camPos.add(camVel);
				
				//render frame
				world.x = -camPos.x;
				world.y = -camPos.y;
				
				camRect = new Rectangle(
					camPos.x - gameWidth / (2*RENDER_SCALE), 
					camPos.y - gameHeight / (2*RENDER_SCALE),  
					gameWidth / RENDER_SCALE,  
					gameHeight / RENDER_SCALE
				);
				
				//a slightly larger rectangle for safe rendering
				renderRect = camRect;
				renderRect.x -= 3*sprites.size;
				renderRect.y -= 3*sprites.size;
				renderRect.width += 2 * 3*sprites.size;
				renderRect.height += 2 * 3*sprites.size;
				
				map.renderRect(renderRect);
				debugPanel.chunks = map.chunksRendered;
			
			}

			//toggle pause menu
			if(!paused && controls.start) {
				paused = true;
				controls.start = false;
			} else if (paused && controls.start) {
				paused = false;
				controls.start = false;
			}
			
			//toggle debug panel
			if(!debug && controls.debug) {
				debug = true;
				controls.debug = false;
				debugPanel.visible = true;
			} else if (debug && controls.debug) {
				debug = false;
				controls.debug = false;
				debugPanel.visible = false;
			}
			
			//toggle console
			if(!console && controls.console) {
				console = true;
				controls.console = false;
				traceConsole.visible = true;
			} else if (console && controls.console) {
				console = false;
				controls.console = false;
				traceConsole.visible = false;
			}
			
			//update debugPanel
			debugPanel.grounded = player.grounded;
			debugPanel.hugging = player.hugging;
			debugPanel.render();
			
		}
		
		//TODO: fps counter
	
	}
	
	private function buildGame():Void {
		
		var spawn:Point = asPoint(map.spawn);
		
		cam.scaleX = cam.scaleY = RENDER_SCALE;
		cam.x = gameWidth / 2;
		cam.y = gameHeight / 2;
		
		drawBackground();
		
		player = new Player(this, sprites, map);
		player.x = spawn.x;
		player.y = spawn.y;
		
		world.addChild(player);
		
		camPos = new Point(player.x, player.y);
		camVel = new Point(0, 0);
		
	}
	
	private function displayFrames():Void {
		debugPanel.fps = fps;
	}
	
	private function drawBackground():Void {
		
		var gradientMatrix:Matrix = new Matrix();
		gradientMatrix.createGradientBox( gameWidth, gameHeight, Math.PI/2, 0, 0 );
	
	    bg.graphics.beginGradientFill(GradientType.LINEAR, [0x77ddff, 0x0099ff], [1, 1], [0, 0xff], gradientMatrix, SpreadMethod.PAD );
	   	bg.graphics.drawRect(0, 0, gameWidth, gameHeight);
	   	bg.graphics.endFill();

	}
	
	private function asPoint(c:Coord):Point {
		return new Point((c.x + .5) * sprites.size, (c.y + .5) * sprites.size);
	}
	
	private function asCoord(p:Point):Coord {
		return new Coord(Math.floor(p.x / sprites.size), Math.floor(p.y / sprites.size));
	}

}

