/*

Map.hx

Map class

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013


*/

package fi.zuigeproductions.nme.tuba;

import nme.display.Sprite;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.PixelSnapping;
import nme.geom.Rectangle;
import nme.geom.Point;
import nme.events.Event;

import nme.Lib;
import nme.Assets;

import fi.zuigeproductions.nme.tuba.Tuba;
import fi.zuigeproductions.nme.tuba.SpriteSheet;

class Map extends Sprite {
	
	public static var CHUNK_WIDTH = 5; 
	public static var CHUNK_HEIGHT = 9;
	
	public var loaded:Bool = false;
	public var spawn:Coord;
	public var chunksRendered:Int = 0;
	
	private static var transparent:Int = 0;

	private var sprites:SpriteSheet;
	private var mapData:BitmapData; 
	
	private var map:Array<Array<Block>>;
	private var chunks:Array<Bitmap>;
	
	private var parseTime:Int;
	private var parseX:Int;
	private var parseY:Int;

	public function new (mapPath:String, sprites:SpriteSheet) {
		
		super(); 

		this.sprites = sprites;
		map = new Array();
		chunks = new Array();
		
		loadMap(mapPath);

	}		
	
	private function loadMap(mapPath:String):Void {
	
		//load map from assets
		mapData = Assets.getBitmapData(mapPath);		
		addEventListener (Event.ENTER_FRAME, parseMap);
		parseX = parseY = 0;
		
	}
	
	private function parseMap(e:Event):Void {
		
		parseTime = Lib.getTimer();
		
		while((Lib.getTimer() - parseTime) <= 1000/Tuba.GAME_FPS) {
			
			if(parseX >= mapData.width) {
			
				//next row
				parseX = 0;
				parseY++;
				
			}
			
			if(parseY >= mapData.height) {
				
				//map parsed
				loaded = true;
				this.removeEventListener(Event.ENTER_FRAME, parseMap);
				break;
				
			}
		
			if(parseY == 0) 
				map[parseX] = new Array(); //init the columns
			
			
			if(map[parseX][parseY] == null) {
			
				//set block if hasn't already been set
				
				var type:BlockType = readType(mapData.getPixel(parseX, parseY));
				
				var sprite:Sprites = NONE;
				var material:Material = HOLLOW;	
				
				if(type == BRICK_GRASS) {
					
					material = SOLID;
					sprite = BRICK_GRASS_0;
					if(getBlock(new Coord(parseX, parseY-1)).material != SOLID) 
						sprite = ((parseX*314 + parseY*3) % 15 == 0) ? BRICK_GRASS_2 : BRICK_GRASS_1; //grass block
					
				}
				
				if(type == BRICK_SNOW) {
					
					material = SOLID;
					sprite = BRICK_SNOW_0;
					if(getBlock(new Coord(parseX, parseY-1)).material != SOLID) 
						sprite = ((parseX*314 + parseY*3) % 15 == 0) ? BRICK_SNOW_2 : BRICK_SNOW_1; //grass block
					
				}
				
				if(type == BRICK_AUTUMN) {
					
					material = SOLID;
					sprite = BRICK_AUTUMN_0;
					if(getBlock(new Coord(parseX, parseY-1)).material != SOLID) 
						sprite = ((parseX*314 + parseY*3) % 15 == 0) ? BRICK_AUTUMN_2 : BRICK_AUTUMN_1; //grass block
					
				}
				
				if(type == BRICK_PURPLE) {
					
					material = SOLID;
					sprite = BRICK_PURPLE_0;
					
				}
				
				if(type == TREE0) {
					
					material = HOLLOW;
					sprite = TREE0_12;
					
					//set surrounding blocks
					setBlock(new Coord(parseX - 1, parseY - 2), new Block(type, TREE0_00, SEMI_SOLID));
					setBlock(new Coord(parseX + 0, parseY - 2), new Block(type, TREE0_10, SEMI_SOLID));
					setBlock(new Coord(parseX + 1, parseY - 2), new Block(type, TREE0_20, SEMI_SOLID));
					
					setBlock(new Coord(parseX - 1, parseY - 1), new Block(type, TREE0_01, HOLLOW));
					setBlock(new Coord(parseX + 0, parseY - 1), new Block(type, TREE0_11, HOLLOW));
					setBlock(new Coord(parseX + 1, parseY - 1), new Block(type, TREE0_21, HOLLOW));
					
					setBlock(new Coord(parseX - 1, parseY - 0), new Block(type, TREE0_02, HOLLOW));
					setBlock(new Coord(parseX + 1, parseY - 0), new Block(type, TREE0_22, HOLLOW));
					
				}
				
				if(type == TREE1) {
					
					material = HOLLOW;
					sprite = TREE1_12;
					
					//set surrounding blocks
					setBlock(new Coord(parseX - 1, parseY - 2), new Block(type, TREE1_00, SEMI_SOLID));
					setBlock(new Coord(parseX + 0, parseY - 2), new Block(type, TREE1_10, SEMI_SOLID));
					setBlock(new Coord(parseX + 1, parseY - 2), new Block(type, TREE1_20, SEMI_SOLID));
					
					setBlock(new Coord(parseX - 1, parseY - 1), new Block(type, TREE1_01, HOLLOW));
					setBlock(new Coord(parseX + 0, parseY - 1), new Block(type, TREE1_11, HOLLOW));
					setBlock(new Coord(parseX + 1, parseY - 1), new Block(type, TREE1_21, HOLLOW));
					
					setBlock(new Coord(parseX - 1, parseY - 0), new Block(type, TREE1_02, HOLLOW));
					setBlock(new Coord(parseX + 1, parseY - 0), new Block(type, TREE1_22, HOLLOW));
					
				}
				
				if(type == TREE2) {
					
					material = HOLLOW;
					sprite = TREE2_12;
					
					//set surrounding blocks
					setBlock(new Coord(parseX - 1, parseY - 2), new Block(type, TREE2_00, SEMI_SOLID));
					setBlock(new Coord(parseX + 0, parseY - 2), new Block(type, TREE2_10, SEMI_SOLID));
					setBlock(new Coord(parseX + 1, parseY - 2), new Block(type, TREE2_20, SEMI_SOLID));
					
					setBlock(new Coord(parseX - 1, parseY - 1), new Block(type, TREE2_01, HOLLOW));
					setBlock(new Coord(parseX + 0, parseY - 1), new Block(type, TREE2_11, HOLLOW));
					setBlock(new Coord(parseX + 1, parseY - 1), new Block(type, TREE2_21, HOLLOW));
					
					setBlock(new Coord(parseX - 1, parseY - 0), new Block(type, TREE2_02, HOLLOW));
					setBlock(new Coord(parseX + 1, parseY - 0), new Block(type, TREE2_22, HOLLOW));
					
				}
				
				if(type == LEAFPILE0) {
					
					material = HOLLOW;
					sprite = LEAF_PILE_0;
					
				}
				
				if(type == SPAWN) {
					
					spawn = new Coord(parseX, parseY - 1); //set map spawn
					sprite = NONE;
					
				}				
				
				setBlock(new Coord(parseX, parseY), new Block(type, sprite, material));
			
			}
			
			parseX++;
		
		}
		
	}
	
	private function renderChunk(coord:Coord) {
		
		//top-left corner of chunk
		var chunkPos:Coord = new Coord(
			Math.floor(coord.x / CHUNK_WIDTH) * CHUNK_WIDTH,  
			Math.floor(coord.y / CHUNK_HEIGHT) * CHUNK_HEIGHT);
		
		var dummyChunk:Sprite = new Sprite(); //temporary drawing container for chunk
		var dummy:Bitmap;
		
		var block:Block;
		var sprite:BitmapData;
	
		for(y in 0...CHUNK_HEIGHT) {
			for(x in 0...CHUNK_WIDTH) {
				
				block = getBlock(new Coord(chunkPos.x + x, chunkPos.y + y));
				
				if(block.sprite == NONE) continue; //don't render empty sprites

				sprite = sprites.loadSprite(block.sprite);

				dummy = new Bitmap(sprite);
				dummy.x = x*sprites.size;
				dummy.y = y*sprites.size;

				//place block
				dummyChunk.addChild(dummy);
				
			}
		
		}
		
		var chunkdata:BitmapData = new BitmapData(
			CHUNK_WIDTH*sprites.size, 
			CHUNK_HEIGHT*sprites.size, 
			true, 
			transparent
		);
		
		chunkdata.draw(dummyChunk); //draw to actual chunk
		dummyChunk = null; //remove dummy
		
		var chunk:Bitmap = new Bitmap(chunkdata);
		
		//smoothing
		//chunk.smoothing = Tuba.BITMAP_SMOOTHING;
		chunk.pixelSnapping = PixelSnapping.ALWAYS;
		
		//position chunk
		chunk.x = chunkPos.x * sprites.size;
		chunk.y = chunkPos.y * sprites.size;
		
		//display and store chunk
		addChild(chunk);
		chunks.push(chunk);
		
	}

	public function renderRect(bounds:Rectangle):Void {
		
		//remove chunks out of rect
		var i:Int = 0;
		while (i < chunks.length) {
			if(!chunks[i].getBounds(this).intersects(bounds)) {
				removeChild(chunks[i]);
				chunks[i] = null;
				chunks.splice(i, 1);
				continue;
			}
			i++;
		}
		
		var x:Int;
		var y:Int;
		
		y = 0;
		while ((y-CHUNK_HEIGHT)*sprites.size <= bounds.height) {
			x = 0;
		
			while ((x-CHUNK_WIDTH)*sprites.size <= bounds.width) {	
		
				var pos:Coord = new Coord(Math.floor(bounds.x / sprites.size) + x, Math.floor(bounds.y / sprites.size) + y);
				
				if (x*sprites.size > bounds.width) 
					pos.x = Math.floor((bounds.x + bounds.width) / sprites.size);
					
				if (y*sprites.size > bounds.height)
					pos.y = Math.floor((bounds.y + bounds.height) / sprites.size); 
					
				if(!isRendered(pos)) {
					renderChunk(pos);
				}
				
				x += CHUNK_WIDTH;
			}
			
			y += CHUNK_HEIGHT;
		}
		
		chunksRendered = chunks.length;
		
		//check if newly created chunks would get deleted right away
		//delete chunks out of rect

/*		i = 0;
		
		while (i < chunks.length) {
			
			if(!chunks[i].getBounds(this).intersects(bounds)) {
				
				removeChild(chunks[i]);
				chunks[i] = null;
				chunks.splice(i, 1);
				continue;
				trace("Discrepancy detected");
			}
			i++;
			
		}*/

	}	
	
	//sets and overrides a block to the map coordinate
	public function setBlock(coord:Coord, block:Block):Void {
	
		if(map[coord.x] != null) 
			map[coord.x][coord.y] = block;
		
	}
	
	//reads map array and returns block at coordinate
	public function getBlock(coord:Coord):Block {
		
		if(coord.x < 0 || coord.x >= mapData.width || coord.y < 0 || coord.y >= mapData.height) {
			//outside of level
			return new Block(BRICK_PURPLE, BRICK_PURPLE_0, SOLID);
		}

		return map[coord.x][coord.y];

	}
	
	//conversions between Point and Coord 	
	public function asPoint(c:Coord):Point {
		return new Point((c.x + .5) * sprites.size, (c.y + .5) * sprites.size);
	}
	public function asCoord(p:Point):Coord {
		return new Coord(Math.floor(p.x / sprites.size), Math.floor(p.y / sprites.size));
	}
	
	//checks if the chunk containing the coord is already rendered
	private function isRendered(coord:Coord):Bool {
		
		for (c in chunks) {
			if(c.getBounds(this).containsPoint(asPoint(coord))) 
				return true;
		}
		
		return false;
	}
	
	//converts map colours to block types
	private function readType(b:Int):BlockType {
		
		return switch(b) {
		
			case 0x0000ff : SPAWN;
			
			case 0xffcc33 : BRICK_GRASS;
			case 0xffff99 : BRICK_SNOW;
			case 0xcc9966 : BRICK_AUTUMN;
			case 0xcc99ff : BRICK_PURPLE;
		   
		    case 0x99cc00 : TREE0;
		    case 0x99ff00 : TREE1;
		    case 0xff9933 : TREE2;
		    
		    case 0x33cc33 : LEAFPILE0;
		    default: EMPTY;
		    
		}
	}

}

//block types
enum BlockType {

    SPAWN;
    
    BRICK_GRASS;
    BRICK_SNOW;
    BRICK_AUTUMN;
    BRICK_PURPLE;
   
    TREE0;
    TREE1;
    TREE2;
    
    LEAFPILE0;
    
    EMPTY;

}

//block materials
enum Material {
	
	SOLID;
	SEMI_SOLID;
	HOLLOW;

}

//blocks stored in map[][] Array
class Block {

	public var type:BlockType;
	public var sprite:Sprites;
	public var material:Material;

	public function new (type:BlockType, ?sprite:Sprites, ?material:Material):Void {
		
		this.type = type;
		this.sprite = sprite;
		this.material = material;
		
	}
	
}

//map coordinate object similar to geom.Point
class Coord {

	public var x:Int;
	public var y:Int;
	
	public function new (x:Int, y:Int) {
		this.x = x;
		this.y = y;
	}
	
}
