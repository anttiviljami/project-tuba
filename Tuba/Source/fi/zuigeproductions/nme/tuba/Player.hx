/*

Player.hx

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013


*/

package fi.zuigeproductions.nme.tuba;

import nme.display.Sprite;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.PixelSnapping;

import nme.geom.Rectangle;
import nme.geom.Point;
import nme.geom.Matrix;
import nme.events.Event;

import StdTypes;

import nme.Lib;
import nme.Assets;

import fi.zuigeproductions.nme.tuba.Tuba;
import fi.zuigeproductions.nme.tuba.Game;
import fi.zuigeproductions.nme.tuba.Map;
import fi.zuigeproductions.nme.tuba.SpriteSheet;
import fi.zuigeproductions.nme.tuba.Controls;

class Player extends Sprite {
	
	public static var JUMP:Float = 7;
	public static var DOUBLE_JUMPS:Bool = false;
	public static var WALL_JUMPS:Bool = true;
	public static var SPRINT:Float = 6;
	public static var WALK:Float = 3.5;
	
	public var vel:Point;
	public var position:Point;
	public var mapPosition:Point;
	
	public var topleft:Point;
	public var topright:Point;
	public var upperleft:Point; 
	public var upperright:Point;
	public var lowerleft:Point;
	public var lowerright:Point;
	public var bottomleft:Point;
	public var bottomright:Point;
	
	public var look:Point;
	
	public var grounded:Bool = false;
	public var hugging:Bool = false;
	
	private static var transparent:Int = 0;
			
	private var game:Game;
	private var map:Map;
	
	private var sprites:SpriteSheet;
	private var sprite:BitmapData;
	private var graphic:Bitmap;
	private var animate:BitmapData;
	private var frame:Int;
	private var last:Int = 0;
	
	private var animation:Sprites;
	private var previousAnimation:Sprites;
	private var animationSpeed:Int;
	
	private var ticks:Int = 0;
	
	private var facing:Bool = true; //true for right, false for left
	
	private var falling:Bool = false;
	private var doublejump:Bool = false;
	private var descend:Bool = false;
	private var parkour:Bool = false;
	
	private var jumped:Bool = false;
	private var jumptimer:Int = 0;
	private var lastjump:Bool = false;
	private var speed:Float;
	private var jumptreshold:Int = 8;
	
	private var below:Material;
	private var infront:Material;
	
	public function new (game:Game, sprites:SpriteSheet, map:Map) {
		
		super();
		
		this.game = game;
		this.sprites = sprites;
		this.map = map;
		
		this.addEventListener(Event.ENTER_FRAME, this_onEnterFrame);
		
		graphic = new Bitmap();
		addChild(graphic);
		graphic.x = - .5 * sprites.size;
		graphic.y = - .5 * sprites.size;
		
		vel = new Point(0, 0);
		look = new Point(0, 0);
		
	}		
	
	public function render():Void {
			
		sprite = sprites.loadSprite(animation);
		
		animate = new BitmapData(sprites.size, sprites.size * 2, true, transparent);
		//animate = new BitmapData(sprites.size, sprites.size * 2);
		
		//frame = Math.floor(ticks * animationSpeed / game.gameFps) % (sprite.width/sprites.size);
		
		if( (ticks - last) > Math.floor(game.gameFps / animationSpeed) ) {
			++frame;
			last = ticks;	
		}
		
		if (frame >= sprite.width/sprites.size) 
				frame = 0;
				
		if(animation != previousAnimation)
			frame = 0; //start new animation from first frame
		
		animate.draw(sprite, new Matrix(1, 0, 0, 1, -sprites.size * frame, facing ? 0 : -2 * sprites.size));
		graphic.bitmapData = animate;
		
		//graphic.smoothing = Tuba.BITMAP_SMOOTHING;
		graphic.pixelSnapping = PixelSnapping.ALWAYS;
		previousAnimation = animation;
		
	}
	
	public function move():Void {
		
		if(position == null) {
			position = new Point(this.x, this.y);
		}
		
		mapPosition = new Point(position.x / sprites.size, position.y / sprites.size);
		game.debugPanel.playerPosition = mapPosition;
		
		//gravity
		vel.y += Game.GRAVITY;	
	
		//check if falling
		falling = vel.y > 0;
		
		//tether velocity
		if(Math.abs(vel.x) > .9 * sprites.size) 
			vel.x = vel.x/Math.abs(vel.x) * .9 * sprites.size;
		
		if(Math.abs(vel.y) > sprites.size) 
			vel.y = vel.y/Math.abs(vel.y) * .9 * sprites.size;
		
		//collisions
		checkCollisions();
		
		//move player
		position.x += vel.x;
		position.y += vel.y;
		
		//if(vel.y < -1) 
			//animation = PLAYER_JUMPING;
			
		//prevent small bouncing
		if(grounded) 
			vel.y = 0;
			
		if(hugging)
			vel.x = 0;
			
		//reset double jump when player hits ground
		if(grounded)
			doublejump = false;
		
		//render with pixel snapping
		this.x = Math.floor(this.position.x);
		this.y = Math.floor(this.position.y);
	
	}
	
	public function controlPlayer(controls:Controls):Void {
			
		descend = false;
		
		animationSpeed = 6;
		animation = PLAYER_STANDING;
		
		look.x = 0;
		look.y = 0;
		
		speed = controls.sprint ? SPRINT : WALK;
		
		if(controls.horizontalAxis < 0) {
			
			if(vel.x > speed*controls.horizontalAxis)
				vel.x -= grounded ? .6  : .3;
			else 
				vel.x = speed*controls.horizontalAxis;
							
			facing = false; //LEFT
			animationSpeed = 4 + Math.round(Math.abs(vel.x)*8/speed);
			animation = PLAYER_RUNNING;
			
			look.x = -1;
		} 
		
		else if(controls.horizontalAxis > 0) {
			
			if(vel.x < speed*controls.horizontalAxis) 
				vel.x += grounded ? .6 : .3;
			else 
				vel.x = speed*controls.horizontalAxis;
			
			facing = true; //RIGHT
			animationSpeed =  4 + Math.round(Math.abs(vel.x)*8/speed);
			animation = PLAYER_RUNNING;
			
			look.x = 1;
		}
		
		else {
			//friction
			if(!grounded) {
				if(Math.abs(vel.x) > .1)
					this.vel.x -= .1 * Math.abs(vel.x)/vel.x;
				else
					this.vel.x = 0;
			} else 
				this.vel.x -= vel.x*.3;
		}
		
		if(controls.verticalAxis < 0) {
						
			//look.y = -2;	
		} 
		
		if (controls.parkour && parkour && grounded && hugging) {
			if(parkour && grounded && hugging) {
				
				this.vel.y = -5;
			
			}
		}
		
		
		else if(controls.verticalAxis > 0) {
			
			//look.y = 2.5;
			descend = true;
			
		} 
		
		if(controls.jump) {
				
			//jump
			jump();
			
			if(vel.y < 0)
				animation = PLAYER_JUMPING;
			
		} 
		
		else {
		
			jumped = false;
		
			if(vel.y < 0) {
				//stop jumping
				vel.y += .4;
				if(vel.y > 0) vel.y = 0;	
			}
				
		}
		
		lastjump = controls.jump;

	}
	
	private function jump():Void {
	
		if(grounded && ((ticks - jumptimer < jumptreshold) || !lastjump)) {
			vel.y = -JUMP;
			doublejump = true;
			jumped = true;
		} 
		
		else if(WALL_JUMPS && hugging && !grounded && (ticks - jumptimer < jumptreshold) && !jumped) {
			vel.y = -JUMP;
			vel.x = facing ? -JUMP*.75 : JUMP*.75 ;
			doublejump = false;
			jumped = true;
		}
		
		else if(DOUBLE_JUMPS && doublejump && !lastjump) {
			vel.y = -JUMP;
			doublejump = false;
			jumped = true;
		}  
		
		if(!lastjump)
			jumptimer = ticks;
			
	}
	
	private function checkCollisions():Void {
		
		//collisions
		grounded = false;
		hugging = false;
		parkour = false;
		
		//points of contact
		bottomright = new Point(position.x + .5 * sprites.size - 1, position.y + 1.5 * sprites.size);
		bottomleft = new Point(position.x - .5 * sprites.size + 1, position.y + 1.5 * sprites.size);
		lowerright = new Point(position.x + .5 * sprites.size, position.y + 1 * sprites.size);
		lowerleft = new Point(position.x - .5 * sprites.size, position.y + 1 * sprites.size);
		topright = new Point(position.x + .5 * sprites.size, position.y + 0 * sprites.size);
		topleft = new Point(position.x - .5 * sprites.size, position.y + 0 * sprites.size);
		
		/*//forced crouch
		if(map.getBlock(map.asCoord(position)).material == SOLID) {
			descend = true;
		}
		
		if(descend) {
			topright = new Point(position.x + .5 * sprites.size, position.y + .5 * sprites.size);
			topleft = new Point(position.x - .5 * sprites.size, position.y + .5 * sprites.size);
		}*/
		
		if(vel.x != 0) {
		
			//clear for parkour maneuver
			infront = map.getBlock(map.asCoord(position.add(new Point(sprites.size * Math.abs(vel.x)/vel.x, 0)))).material;
			if(infront != SOLID)
				parkour = true;
		
			//left&right collisions
			if(map.getBlock(map.asCoord(bottomright.add(new Point(vel.x, -1)))).material == SOLID && vel.x > 0) {
				vel.x = map.asPoint(map.asCoord(bottomright.add(new Point(vel.x, -1)))).x - 1 * sprites.size - position.x;
				hugging = true;
			}
			else if(map.getBlock(map.asCoord(bottomleft.add(new Point(vel.x, -1)))).material == SOLID && vel.x < 0) {
				trace("b: " + vel.x);
				vel.x = map.asPoint(map.asCoord(bottomleft.add(new Point(vel.x, -1)))).x + 1 * sprites.size - position.x;
				hugging = true;
				trace("a: " + vel.x);
			}
			else if(map.getBlock(map.asCoord(lowerright.add(new Point(vel.x, 0)))).material == SOLID && vel.x > 0) {
				vel.x = map.asPoint(map.asCoord(lowerright.add(new Point(vel.x, 0)))).x - 1 * sprites.size - position.x;
				hugging = true;
			}
			else if(map.getBlock(map.asCoord(lowerleft.add(new Point(vel.x, 0)))).material == SOLID && vel.x < 0) {
				vel.x = map.asPoint(map.asCoord(lowerleft.add(new Point(vel.x, 0)))).x + 1 * sprites.size - position.x;
				hugging = true;
			}
			else if(map.getBlock(map.asCoord(topright.add(new Point(vel.x, 0)))).material == SOLID && vel.x > 0) {
				vel.x = map.asPoint(map.asCoord(topright.add(new Point(vel.x, 0)))).x - 1 * sprites.size - position.x;
				hugging = true;
			}
			else if(map.getBlock(map.asCoord(topleft.add(new Point(vel.x, 0)))).material == SOLID && vel.x < 0) {
				vel.x = map.asPoint(map.asCoord(topleft.add(new Point(vel.x, 0)))).x + 1 * sprites.size - position.x;
				hugging = true;
			}
		
		}
		
		//top&bottom collisions
		if(!falling && map.getBlock(map.asCoord(topleft.add(new Point(0, vel.y)))).material == SOLID) {
			//bump head
			if(map.asPoint(map.asCoord(topleft.add(new Point(0, vel.y)))).y + .5 * sprites.size - topright.y <= 0) 
				vel.y = map.asPoint(map.asCoord(topleft.add(new Point(0, vel.y)))).y + .5 * sprites.size - topright.y;
		}
		
		if(!falling && map.getBlock(map.asCoord(topright.add(new Point(-1, vel.y)))).material == SOLID) {
			//bump head
			if(map.asPoint(map.asCoord(topright.add(new Point(-1, vel.y)))).y + .5 * sprites.size - topright.y <= 0) 
				vel.y = map.asPoint(map.asCoord(topright.add(new Point(-1, vel.y)))).y + .5 * sprites.size - topright.y;
		}
				
		below = map.getBlock(map.asCoord(bottomleft.add(vel))).material;
		if(falling && (below == SOLID || (below == SEMI_SOLID && !descend))) {
			//left foot touches ground
			if(map.asPoint(map.asCoord(bottomleft.add(new Point(0, vel.y)))).y - 2 * sprites.size - position.y >= - vel.y) {
				vel.y = map.asPoint(map.asCoord(bottomleft.add(new Point(0, vel.y)))).y - 2 * sprites.size - position.y;
				grounded = true;
				falling = false;
			}
		}
		
		below = map.getBlock(map.asCoord(bottomright.add(new Point(0, vel.y)))).material;
		if(falling && (below == SOLID || (below == SEMI_SOLID && !descend))) {
			//right foot touches ground
			if(map.asPoint(map.asCoord(bottomright.add(new Point(0, vel.y)))).y - 2 * sprites.size - position.y >= - vel.y) {	
				vel.y = map.asPoint(map.asCoord(bottomright.add(new Point(0, vel.y)))).y - 2 * sprites.size - position.y;
				grounded = true;
				falling = false;
			}
			
		}
		
	}
	
	private function this_onEnterFrame (e:Event) {
	
		ticks++;
	
	}

}
