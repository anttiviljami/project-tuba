/*

SpriteSheet.hx

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013


*/

package fi.zuigeproductions.nme.tuba;

import nme.Lib;
import nme.Assets;

import nme.display.Sprite;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.geom.Rectangle;
import nme.geom.Matrix;

class SpriteSheet {
	
	public var size:Int; //basic tile unit
	
	private static var transparent:Int = 0;

	private var spriteSheet:BitmapData;
	
	public function new (path:String, size:Int) {
		
		this.spriteSheet = Assets.getBitmapData (path);
		this.size = size;
		
	}	
	
	public function loadSprite(s:Sprites):BitmapData {
		
		var rect:Rectangle = spriteRect(s); 
		
		var sprite:BitmapData = new BitmapData(
			Math.floor(rect.width * size), 
			Math.floor(rect.height * size), 
			true, 
			transparent
		);
		
		sprite.draw(
			spriteSheet, 
			new Matrix(1, 0, 0, 1, -rect.x * size, -rect.y * size), 
			null, 
			null, 
			new Rectangle(0, 0, rect.width * size, rect.height * size)
		);
		
		return sprite;
	
	}
	
	private function spriteRect(s:Sprites):Rectangle {
	
		return switch(s) {
			
		    case NONE : new Rectangle(0, 0, 0, 0);
		    
		    case PLAYER_STANDING : new Rectangle(0, 0, 4, 4);
		    case PLAYER_RUNNING : new Rectangle(0, 4, 6, 4);
		   	case PLAYER_JUMPING : new Rectangle(0, 8, 1, 4);
		  	
		  	case BRICK_GRASS_0 : new Rectangle(0, 21, 1, 1);
		   	case BRICK_GRASS_1 : new Rectangle(1, 21, 1, 1);
		   	case BRICK_GRASS_2 : new Rectangle(2, 21, 1, 1);
		   	
		   	case BRICK_SNOW_0 : new Rectangle(0, 25, 1, 1);
		   	case BRICK_SNOW_1 : new Rectangle(1, 25, 1, 1);
		   	case BRICK_SNOW_2 : new Rectangle(2, 25, 1, 1);
		   	
		   	case BRICK_AUTUMN_0 : new Rectangle(0, 29, 1, 1);
		   	case BRICK_AUTUMN_1 : new Rectangle(1, 29, 1, 1);
		   	case BRICK_AUTUMN_2 : new Rectangle(2, 29, 1, 1);
		   	
		   	case BRICK_PURPLE_0 : new Rectangle(0, 33, 1, 1);
		   
		   	case TREE0_00  : new Rectangle(0 + 0, 18 + 0, 1, 1);
		   	case TREE0_01  : new Rectangle(0 + 0, 18 + 1, 1, 1);
		   	case TREE0_02  : new Rectangle(0 + 0, 18 + 2, 1, 1);
		   	case TREE0_10  : new Rectangle(0 + 1, 18 + 0, 1, 1);
		   	case TREE0_11  : new Rectangle(0 + 1, 18 + 1, 1, 1);
		   	case TREE0_12  : new Rectangle(0 + 1, 18 + 2, 1, 1);
		   	case TREE0_20  : new Rectangle(0 + 2, 18 + 0, 1, 1);
		   	case TREE0_21  : new Rectangle(0 + 2, 18 + 1, 1, 1);
		   	case TREE0_22  : new Rectangle(0 + 2, 18 + 2, 1, 1);
		   	
		   	case TREE1_00  : new Rectangle(0 + 0, 22 + 0, 1, 1);
		   	case TREE1_01  : new Rectangle(0 + 0, 22 + 1, 1, 1);
		   	case TREE1_02  : new Rectangle(0 + 0, 22 + 2, 1, 1);
		   	case TREE1_10  : new Rectangle(0 + 1, 22 + 0, 1, 1);
		   	case TREE1_11  : new Rectangle(0 + 1, 22 + 1, 1, 1);
		   	case TREE1_12  : new Rectangle(0 + 1, 22 + 2, 1, 1);
		   	case TREE1_20  : new Rectangle(0 + 2, 22 + 0, 1, 1);
		   	case TREE1_21  : new Rectangle(0 + 2, 22 + 1, 1, 1);
		   	case TREE1_22  : new Rectangle(0 + 2, 22 + 2, 1, 1);
		   	
		   	case TREE2_00  : new Rectangle(0 + 0, 26 + 0, 1, 1);
		   	case TREE2_01  : new Rectangle(0 + 0, 26 + 1, 1, 1);
		   	case TREE2_02  : new Rectangle(0 + 0, 26 + 2, 1, 1);
		   	case TREE2_10  : new Rectangle(0 + 1, 26 + 0, 1, 1);
		   	case TREE2_11  : new Rectangle(0 + 1, 26 + 1, 1, 1);
		   	case TREE2_12  : new Rectangle(0 + 1, 26 + 2, 1, 1);
		   	case TREE2_20  : new Rectangle(0 + 2, 26 + 0, 1, 1);
		   	case TREE2_21  : new Rectangle(0 + 2, 26 + 1, 1, 1);
		   	case TREE2_22  : new Rectangle(0 + 2, 26 + 2, 1, 1);
		   	
		   	case LEAF_PILE_0 : new Rectangle(3, 28, 1, 1);
		   	
		   	case CLOUD0  : new Rectangle(0, 16, 2, 3);
		   	
		}

	}

}

enum Sprites {

    NONE;    
    
    PLAYER_STANDING;
    PLAYER_RUNNING;
    PLAYER_JUMPING;
    
    BRICK_GRASS_0;
    BRICK_GRASS_1;
    BRICK_GRASS_2;
    
    BRICK_SNOW_0;
    BRICK_SNOW_1;
    BRICK_SNOW_2;
    
    BRICK_AUTUMN_0;
    BRICK_AUTUMN_1;
    BRICK_AUTUMN_2;
	
	BRICK_PURPLE_0;
	
	TREE0_00;
	TREE0_01;
	TREE0_02;
	TREE0_10;
	TREE0_11;
	TREE0_12;
	TREE0_20;
	TREE0_21;
	TREE0_22;
	
	TREE1_00;
	TREE1_01;
	TREE1_02;
	TREE1_10;
	TREE1_11;
	TREE1_12;
	TREE1_20;
	TREE1_21;
	TREE1_22;
	
	TREE2_00;
	TREE2_01;
	TREE2_02;
	TREE2_10;
	TREE2_11;
	TREE2_12;
	TREE2_20;
	TREE2_21;
	TREE2_22;
	
	LEAF_PILE_0;
	
    CLOUD0;
    
}
