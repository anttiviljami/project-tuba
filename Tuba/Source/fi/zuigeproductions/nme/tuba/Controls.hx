/*

Controls.hx

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013


*/

package fi.zuigeproductions.nme.tuba;

import nme.Lib;
import nme.display.Sprite; 

import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.ui.Keyboard;

#if desktop 

//Gamepad support for PC & Mac

import nme.events.JoystickEvent;

#end

 
class Controls extends Sprite {

	public static var deadzone_low:Float = .25;
	public static var deadzone_high:Float = .15;

	public var horizontalAxis:Float = 0;
	public var verticalAxis:Float = 0;
	public var jump:Bool = false;
	public var sprint:Bool = false;
	public var parkour:Bool = false;
	public var start:Bool = false;
	public var console:Bool = false;
	public var debug:Bool = false;
	
	public var axis0:Float = 0;
	public var axis1:Float = 0;
	public var trigger:Bool = false;
	
	public var leftDPad:Bool = false;
	public var rightDPad:Bool = false;
	public var upDPad:Bool = false;
	public var downDPad:Bool = false;
	public var shift:Bool = false;
	
	public function new () {
	
		super();
		
		this.addEventListener (Event.ENTER_FRAME, onEnterFrame);
		
		Lib.stage.addEventListener (KeyboardEvent.KEY_DOWN, onKeyDown);
		Lib.stage.addEventListener (KeyboardEvent.KEY_UP, onKeyUp);
		
		#if desktop
		// No joystick support for flash
		
		Lib.stage.addEventListener (JoystickEvent.AXIS_MOVE, onAxisMove);
		Lib.stage.addEventListener (JoystickEvent.BUTTON_DOWN, onButtonDown);
		Lib.stage.addEventListener (JoystickEvent.BUTTON_UP, onButtonUp);
		
		#end
		
	}
	
	private function onEnterFrame(e:Event):Void {
		
		horizontalAxis = verticalAxis = 0;
		
		if(leftDPad)
			horizontalAxis = -1;
			
		if(rightDPad)
			horizontalAxis = 1;	
			
		if(upDPad)
			verticalAxis = -1;	
			
		if(downDPad)
			verticalAxis = 1;
		
		if(axis0 != 0 || axis1 != 0) {
			horizontalAxis = axis0;
			verticalAxis = axis1;
		}
		
		sprint = trigger || shift;
		
	}
	
	//key state functions
	private function onKeyDown(e:KeyboardEvent):Void {
		
		trace(e.keyCode);
		
		switch(e.keyCode) {
			case Keyboard.A : leftDPad = true;
			case Keyboard.D : rightDPad = true;
			case Keyboard.W : upDPad = parkour = true;
			case Keyboard.S : downDPad = true;
			case Keyboard.SPACE : jump = true;
			case Keyboard.SHIFT : shift = true;
		
			case Keyboard.LEFT : leftDPad = true;
			case Keyboard.RIGHT : rightDPad = true;
			case Keyboard.UP : upDPad = parkour = true;
			case Keyboard.DOWN : downDPad = true;
			case Keyboard.Z : jump = true;
			case Keyboard.X : shift = true;
			
			case Keyboard.ESCAPE : start = true;
			case Keyboard.P : start = true;

			case Keyboard.T : console = true;
		
			case Keyboard.F3 : debug = true;
			case 82 : debug = true;

		}
		
	}
	
	private function onKeyUp(e:KeyboardEvent):Void {
	
		switch(e.keyCode) {
			case Keyboard.A : leftDPad = false;
			case Keyboard.D : rightDPad = false;
			case Keyboard.W : upDPad = parkour = false;
			case Keyboard.S : downDPad = false;
			case Keyboard.SPACE : jump = false;
			case Keyboard.SHIFT : shift = false;
			
			case Keyboard.LEFT : leftDPad = false;
			case Keyboard.RIGHT : rightDPad = false;
			case Keyboard.UP : upDPad = parkour = false;
			case Keyboard.DOWN : downDPad = false;
			case Keyboard.Z : jump = false;
			case Keyboard.X : shift = false;
			
			case Keyboard.ESCAPE : start = false;
			case Keyboard.P : start = false;
			
			case Keyboard.T : console = false;
		
			case Keyboard.F3 : debug = false;
			case 82 : debug = false;

		}
		
	}
	
	#if desktop
    // No joystick support for flash
   
	private function onAxisMove(e:JoystickEvent):Void {
		
		axis0 = axis1 = 0;
		
		if(Math.abs(e.axis[0]) > deadzone_low)  {
			axis0 =  Math.abs(e.axis[0]) * (Math.abs(e.axis[0]) - deadzone_low) / (1 - deadzone_high - deadzone_low) * e.axis[0];
		}
		
		if(Math.abs(e.axis[1]) > deadzone_low) {
			axis1 =  Math.abs(e.axis[1]) * (Math.abs(e.axis[1]) - deadzone_low) / (1 - deadzone_high - deadzone_low) * e.axis[1];
		}
		
		if(Math.abs(axis0) > 1)
			axis0 = Math.abs(axis0)/axis0;
			
		if(Math.abs(axis1) > 1)
			axis1 = Math.abs(axis1)/axis1;
		
		#if windows
		trigger = e.axis[2] > .5; 
		#else
		trigger = e.axis[4] > 0;
		#end		
		
	}
	
	private function onButtonDown(e:JoystickEvent):Void {
		
		trace(e.id);
		
		switch (e.id) {
			#if windows
			case 0 : jump = true;	//A-button
			case 2 : parkour = true; //X-button
			case 7 : start = true; //Start-button
			case 6 : debug = true; //Back-button
			#elseif mac
			case 11 : jump = true; //A-button
			case 13 : parkour = true; //X-button
			case 4 : start = true; //Start-button
			case 5 : debug = true; //Back-button
			#end
		}
		
	}
	
	private function onButtonUp(e:JoystickEvent):Void {
		
		switch (e.id) {
			#if windows
			case 0 : jump = false;	//A-button
			case 2 : parkour = false; //X-button
			case 7 : start = false; //Start-button
			case 6 : debug = false; //Back-button
			#elseif mac
			case 11 : jump = false; //A-button
			case 13 : parkour = false; //X-button
			case 4 : start = false; //Start-button
			case 5 : debug = false; //Back-button
			#end
		}
	
	}
	
	#end

}

