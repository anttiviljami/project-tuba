/*

README.txt

Project Tuba - (Paradise)

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013


*/


CONCEPT:

A 2D Platformer in stunning scenery. A utopian game world to explore. Only to be ruined by the players.

This game is a story about the relationship of the gamer and the game developer.


PLATFORMS: 

OS X, Windows, Flash (Demo?)


IDEAS:

Jetpacks!, 


STORY:

*Game fades in*

"Welcome to the game world, player!"
"I'm very happy to see you here."
"I've been expecting you."

"Do you know how to play?"

*Player is given the option to skip tutorial*

"Let's start with the basics."
"Use the joystick on your gamepad to move around the game world."
"It's fine if you don't have a gamepad. You can just use the directional keys on the keyboard."
"It's not like the game is designed to play with a proper controller."

"Well done, player. We shall move on."
"Use the A-button or the Z-key on the keyboard to jump."
"You can perform a wall jump if you like."
"I'm sure you'll figure it out on your own."

"How lovely!"
"Hold down the left trigger or the shift key to run faster"
"It's slightly awkward on a keyboard, I know."

"I see you're still not using a gamepad."
"Just use the typewriter-like device on your desk if you like. It's completely fine."

"Brilliant! You're just about ready to start having fun on your own."
"I've prepared a task for you I'm sure you'll enjoy."
"Are you ready to hear it, player?"

"Your objective in the game world is to collect coins. What jolly fun!"
"Good luck! I'll be following your progress as you indulge yourself in a world of joyous pastime."
"Have at it!"

"I hope you're enjoying the game."
"This was all built just for you."
"Feel free to explore the game world as you wish"
"I love you."
"Aren't you having just the most fun?"
"Be sure to collect all the coins."

*Player eventually stumbles upon a glitchy spot on the map*

"Oh, I'm sorry. I must've forgot about that bit. Just ignore the missing blocks."
"Where are you going?"
"You're not supposed to go in there!"
"It's a glitch, just ignore it!"

*Player accidentally finds the ability to destroy bricks*

*"Blocks destroyed: 10" appears on the screen*

"Oh no! What has happened?"
"Was that you?"
"Did you just destroy a block?"
"Please stop."
"Why aren't you playing this like you're supposed to?"
"All that work… Why would you do this?"
"I thought you loved me."
"Stop this immediately."

*Player reaches 100 destroyed blocks*

"I must punish you for this."

*Health bar appears over player's head. Sky goes dark.*

"You are no longer welcome here, player."
"Please, go away."

*Blocks around player start changing to lava, spikes and rock*
*The game goes survival mode, player must run to survive*

